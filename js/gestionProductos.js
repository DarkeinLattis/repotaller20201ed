var productosObtenidos;

function getProductos() {
	var url = "https://services.odata.org/V4/NorthWind/NorthWind.svc/Products";
	var req = new XMLHttpRequest();

	req.onreadystatechange = function () {
		if( this.readyState == 4 && this.status == 200 ) {
			//console.table( JSON.parse(this.responseText).value );
			productosObtenidos = this.responseText;
			}
		}
	req.open("GET", url, true);
	req.send();
	}

function procesarProductos() {
	var arrProds = JSON.parse(productosObtenidos);
	var divTabla = document.getElementById("divTablaProductos");
	var tabla = document.createElement("table");
	var cuerpo = document.createElement("tbody");
	//console.table( arrProds.value );

	tabla.classList.add("table");
	tabla.classList.add("table-striped");

	for( var i = 0; i<arrProds.value.length; i++) {
		//console.log(arrProds.value[i].ProductName);
		var nuevaFila = document.createElement("tr");
		var columnaNombre = document.createElement("td");
		columnaNombre.innerText = arrProds.value[i].ProductName;

		var columnaPrecio = document.createElement("td");
		columnaPrecio.innerText = arrProds.value[i].UnitPrice;

		var columnaUnidades = document.createElement("td");
		columnaUnidades.innerText = arrProds.value[i].UnitsInStock;

		nuevaFila.appendChild(columnaNombre);
		nuevaFila.appendChild(columnaPrecio);
		nuevaFila.appendChild(columnaUnidades);

		cuerpo.appendChild(nuevaFila);
		}

	tabla.appendChild(cuerpo);
	divTabla.appendChild(tabla);
	}
