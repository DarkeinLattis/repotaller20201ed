var JSON_obtenido;
function getClientes() {
	var url = "https://services.odata.org/V4/NorthWind/NorthWind.svc/Customers";
	//var url = "https://services.odata.org/V4/NorthWind/NorthWind.svc/Customers?$filter=Country eq 'Germany'";
	var req = new XMLHttpRequest();

	req.onreadystatechange = function () {
		if( this.readyState == 4 )
			if ( this.status == 200 ) {
				//console.table( JSON.parse(this.responseText).value );
				JSON_obtenido =  this.responseText;
				return;
				}
			else
				window.alert("Fallo en petición: Status: " + this.readyState + " ResponseCode: " + this.status);
		}
	req.open("GET", url, true);
	req.send();
	}

function procesarClientes() {
	var arr = JSON.parse(JSON_obtenido);
	var divTabla = document.getElementById("divTablaClientes");
	var tabla = document.createElement("table");
	var cuerpo = document.createElement("tbody");
	//console.table( arr.value );

	var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
	var imgTagP1 = "<img src='https://www.countries-ofthe-world.com/flags-normal/flag-of-";
	var imgTagP2 = ".png' height='30'>";
	tabla.classList.add("table");
	tabla.classList.add("table-striped");

	for( var i = 0; i<arr.value.length; i++) {
		//console.log(arrProds.value[i].ProductName);
		var nuevaFila = document.createElement("tr");
		var columna = document.createElement("td");
		columna.innerText = arr.value[i].ContactName;
		nuevaFila.appendChild(columna);

		columna = document.createElement("td");
		columna.innerText = arr.value[i].City;
		nuevaFila.appendChild(columna);

		columna = document.createElement("td");
		var bandera = document.createElement("img");
		if( arr.value[i].Country == "UK" )
			bandera.src = rutaBandera + "United-Kingdom.png";
		else
			bandera.src = rutaBandera + arr.value[i].Country + ".png";
		//columna.innerHTML = imgTagP1 + arr.value[i].Country + imgTagP2;
		bandera.style.width='70px';
		bandera.style.height='40px';
		columna.appendChild(bandera);
		nuevaFila.appendChild(columna);

		cuerpo.appendChild(nuevaFila);
		}

	tabla.appendChild(cuerpo);
	divTabla.appendChild(tabla);
	}
